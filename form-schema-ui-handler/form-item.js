import helper from './form-item-helper'

export default {
  functional: true,
  props: {
    label: { type: String },
    enableWrapper: { type: Boolean, default: false }
  },
  disableWrappingLabel: true,
  render(h, { data, props, slots, listeners, ...functionalRenderContext }) {
    const children = slots().default
    if (data.field.isArrayField && props.enableWrapper !== true) {
      return children
    }
    const description = props.description || (data.field && data.field.description)
    if (props.label) children.unshift(h('label', props.label))
    if (description) children.push(h(helper, description))
    return h('md-field', {
      props: {
        prop: data.field.attrs.name,
        label: props.label,
        required: data.field.attrs.required
      }
    }, children)
  }
}