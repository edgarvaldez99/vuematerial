import FormItem from './form-item'

export default (component) => ({
  functional: true,
  render(h, { data, props, listeners }) {
    const node = h(component, {
      props: {
        name: data.attrs.name,
        label: data.fieldParent ? props.value : props.label
      },
      on: {
        change(value, event) {
          event.target.checked = value

          listeners.change(event)
        }
      }
    }, data.fieldParent && !data.fieldParent.isArrayField ? '' : props.label)

    if (data.fieldParent) {
      return h(FormItem, {
        field: data.fieldParent || data.field,
        props: props
      }, [node])
    }

    return node
  }
})

// const choice = (tag) => formItem((h, { props, slots }) => h(tag, props.input, slots().default), false)