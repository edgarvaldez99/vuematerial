export default {
  functional: true,
  render(h, { data, props, listeners, slots }) {
    const field = data.field
    const vnode = h('md-select', {
      props: props,
      on: {
        change: [
          listeners.change,
          (value) => {
            const { label } = field.items.find((item) => item.value === value)
console.log("Entró", value)
            vnode.componentInstance.selected.value = value
            vnode.componentInstance.selected.currentLabel = label
          }
        ]
      }
    }, slots().default)

    return vnode
  }
}