import FormItem from './form-item'

export default {
  functional: true,
  render(h, { data, listeners, slots }) {
    return h(FormItem, {
      field: data.field,
      attrs: data.attrs,
      props: {
        label: data.field.label,
        enableWrapper: true
      },
      on: listeners
    }, slots().default)
  }
}