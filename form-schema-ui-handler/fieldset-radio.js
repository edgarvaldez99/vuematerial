import FormItem from './form-item'

export default {
  functional: true,
  render(h, { data, props, listeners, slots }) {
    const nodes = slots().default
    const fieldset = h('el-radio-group', {
      props: props,
      on: {
        input(value) {
          /**
            * Related to http://element.eleme.io/#/en-US/component/radio#radio-events
            * ElementUI send the label value of the chosen radio instead of
            * the radio value. So we need to make an hack to emit the selected
            * element value to the FormSchema instance
            * :-/
            */
          const item = data.field.items.find((item) => item.label === value)

          if (!item) {
            return
          }

          const target = {
            value: item.value
          }

          listeners.input({ target })
        }
      }
    }, nodes)

    return h(FormItem, {
      field: data.field,
      attrs: data.attrs,
      props: {
        label: props.label,
        value: props.value,
        enableWrapper: true
      }
    }, [fieldset])
  }
}