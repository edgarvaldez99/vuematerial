export default {
  props: {
    className: {
      type: String,
      default: "md-helper-text"
    },
    description: {
      type: String,
      default: ""
    }
  },
  render(createElement) {
    const className = this.className
    const slot = this.description || this.$slots.default
    return createElement('small', {
      attrs: {
        class: className,
      }
    }, slot)
  }
}
