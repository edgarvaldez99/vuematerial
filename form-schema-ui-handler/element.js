import FormItem from './form-item'

export default (component) => ({
  functional: true,
  props: ['type', 'label', 'value'],
  // Parameters for render: is a object of the type FunctionalRenderContext with properties:
  //    children: [VNode], data: Object, injections: any, listeners: Object<Function>, parent: VueComponent, props: Object, slots: Function
  render(h, { data, props, listeners, slots }) {

    const nodes = [
      h(component, {
        field: data.field,
        props: {
          ...data.attrs,
          ...props
        },
        on: {
          ...listeners,
          input(value) {
            const target = nodes[0].elm

            target.value = value
            props.value = value

            listeners.input({ target })
          },
          change(value, event) {
            const target = nodes[0].elm

            target.value = value
            props.value = value

            listeners.change({ target })
          }
        }
      }, slots().default)
    ]

    return h(FormItem, {
      field: data.field,
      props: props,
      on: listeners
    }, nodes)
  }
})
