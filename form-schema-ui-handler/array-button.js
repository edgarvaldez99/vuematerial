export default {
  functional: true,
  render(h, { listeners }) {
    return h('el-button', {
      on: listeners,
      attrs: {
        type: 'text'
      }
    }, 'Add more item')
  }
}