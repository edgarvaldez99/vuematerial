export default {
  functional: true,
    render(h, { slots }) {
    return h('md-alert', {
      props: {
        type: 'error'
      }
    }, slots().default)
  }
}