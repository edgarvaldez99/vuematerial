import FormItem from './form-item'

export default {
  functional: true,
  render(h, { data, props, listeners, slots }) {
    const fieldset = h('fieldset', {
      props: props,
      on: {
        change(value) {
          console.log('èèèèèèèèè>', value)
        },
        input(value) {
          /**
            * Related to http://element.eleme.io/#/en-US/component/radio#radio-events
            * ElementUI send the label value of the chosen radio instead of
            * the radio value. So we need to make an hack to emit the selected
            * element value to the FormSchema instance
            * :-/
            */
          const item = field.items.find(({ label }) => label === value)

          const target = {
            value: item.value
          }

          props.value = item.value

          listeners.input({ target })
        }
      }
    }, slots().default)

    return h(FormItem, {
      field: data.field,
      attrs: data.attrs,
      props: {
        label: data.field.label,
        enableWrapper: true
      },
      on: listeners
    }, [fieldset])
  }
}