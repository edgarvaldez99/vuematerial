export default {
  functional: true,
  render(h, { props, listeners, slots }) {
    return h('form', {
      props: {
        model: props.value,
        labelWidth: '120px'
      },
      on: listeners
    }, slots().default)
  }
}