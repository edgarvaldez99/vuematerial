export default (inputType) => ({
  functional: true,
  render(h, { data, props, listeners, slots }) {
    return h('md-input', {
      props: {
        ...data.attrs,
        type: inputType,
        value: props.value,
        label: props.label
      },
      on: listeners
    }, slots().default)
  }
})
