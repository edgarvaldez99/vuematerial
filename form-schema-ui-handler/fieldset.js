import FieldsetCheckbox from './fieldset-checkbox'
import FieldsetDefault from './fieldset-default'
import FieldsetInput from './fieldset-input'
import FieldsetRadio from './fieldset-radio'

export default {
  functional: true,
  render(h, { data, props, listeners, slots }) {
    switch (data.field.attrs.type) {
      case 'radio':
        return h(FieldsetRadio, data, slots().default)

      case 'checkbox':
        return h(FieldsetCheckbox, data, slots().default)
    }

    if (data.newItemButton) {
      return h(FieldsetInput, data, slots().default)
    }

    return h(FieldsetDefault, data, slots().default)
  }
}