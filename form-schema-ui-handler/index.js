import Form from './form'
import Element from './element'
import Input from './input'
import Error from './error'
import ElCheckbox from './checkbox'
import ElSelect from './select'
import Option from './option'
import Radio from './radio'
import Fieldset from './fieldset'

const Text = Element('md-input')
const Textarea = Element(Input('textarea'))
const Checkbox = ElCheckbox('el-checkbox')
const File = Element(Input('file'))
const Select = Element(ElSelect)

export default (Components) => {
  const components = new Components()

  components.set('form', Form)
  components.set('text', Text)
  components.set('checkbox', Checkbox)
  components.set('radio', Radio)
  components.set('file', File)
  components.set('select', Select)
  components.set('option', Option)
  components.set('hidden', 'input')
  components.set('textarea', Textarea)
  components.set('fieldset', Fieldset)
  components.set('error', Error)

  return components
}
