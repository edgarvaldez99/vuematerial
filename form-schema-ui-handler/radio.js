export default {
  functional: true,
  render(h, { data, props, listeners }) {
    return h('el-radio', {
      props: {
        name: data.attrs.name,
        label: props.label,
        value: props.value
      }
    }, props.label)
  }
}