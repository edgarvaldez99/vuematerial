export default {
  functional: true,
  render(h, { props, listeners }) {
    const label = props.label
    return h('md-option', {
      props: {
        label: label,
        value: props.value
      },
      on: listeners
    }, label)
  }
}