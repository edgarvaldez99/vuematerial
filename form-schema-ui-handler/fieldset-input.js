import FormItem from './form-item'
import ArrayButton from './array-button'

export default {
  functional: true,
  render(h, { data, listeners, slots }) {
    const nodes = slots().default || []

    nodes.push(h('div', [
      h(ArrayButton, data.newItemButton)
    ]))

    return h(FormItem, {
      field: data.field,
      attrs: data.attrs,
      props: {
        label: data.field.label,
        enableWrapper: true
      },
      on: listeners
    }, nodes)
  }
}